" Joiner. Ability to join lines in a text object.
"
" Joiner provides several bindings, with the leader being configurable. This
" documentation assums <C-j> as the leader.
"
" <C-j>s<text_object><string>: join the lines in <text_object> with <string>.
"                              The lines are stripped of whitesapce before being
"                              joined
"
" <C-j>S<text_object><string>: join the lines in <text_object> with <string>.
"                              The lines are joined verbatim.
"
" <C-j><text_object>:  join the lines in <text_object>. The lines are stripped
"                      before being joined.
"
" There are Visual counterparts to these.

if ! exists('g:joiner_include_bindings')
  let g:joiner_include_bindings = 1
endif

if ! exists('g:joiner_leader')
  let g:joiner_leader = '<C-j>'
endif

noremap  <silent> <Plug>(joiner)s <cmd>let g:JoinerJoinText=function('joiner#join_with_string')<cr><cmd>set operatorfunc=joiner#do<cr>g@
noremap  <silent> <Plug>(joiner)S <cmd>let g:JoinerJoinText=function('joiner#join_with_string_literal')<cr><cmd>set operatorfunc=joiner#do<cr>g@
noremap  <silent> <Plug>(joiner)  <cmd>let g:JoinerJoinText=function('joiner#default_join')<cr><cmd>set operatorfunc=joiner#do<cr>g@

vnoremap <silent> <Plug>(joiner)s       <cmd>let g:JoinerJoinText=function('joiner#join_with_string')<cr>gv<cmd>call joiner#do(visualmode(), 1)<cr>
vnoremap <silent> <Plug>(joiner)S       <cmd>let g:JoinerJoinText=function('joiner#join_with_string_literal')<cr><cmd>call joiner#do(visualmode(), 1)<cr>
vnoremap <silent> <Plug>(joiner)<space> <cmd>let g:JoinerJoinText=function('joiner#default_join')<cr><cmd>call joiner#do(visualmode(), 1)<cr>

if g:joiner_include_bindings
  exec printf("vnoremap <silent> %s <Plug>(joiner)", g:joiner_leader)
  exec printf("noremap <silent> %s <Plug>(joiner)", g:joiner_leader)
endif
