noremap  <silent> <plug>(run-hints) <cmd>call hints#runHints("")<cr>
onoremap <silent> <plug>(run-hints) <cmd>call hints#runHints("o")<cr>
vnoremap <silent> <plug>(run-hints) <cmd>call hints#runHints("")<cr>
