" Fall through whitespace.
noremap <expr> <silent> <leader>k fall#fall('k', '^\s*$')
noremap <expr> <silent> <leader>j fall#fall('j', '^\s*$')

" Text object to fall through whitepacea
onoremap <silent> <leader>k <cmd>exec "normal! V" . fall#fall('k', '^\s*$')<cr>
onoremap <silent> <leader>j <cmd>exec "normal! V" . fall#fall('j', '^\s*$')<cr>

" Text object to fall though whitespace, but exclude the last line.
onoremap <silent> i<leader>k <cmd>exec "normal! V" . fall#fall('k', '^\s*$') . "j"<cr>
onoremap <silent> i<leader>j <cmd>exec "normal! V" . fall#fall('j', '^\s*$') . "k"<cr>

" Text object to fall though whitespace, but exclude the last line.
onoremap <silent> i<leader>k <cmd>exec "normal! V" . fall#fall('k', '^\s*$') . "j"<cr>
onoremap <silent> i<leader>j <cmd>exec "normal! V" . fall#fall('j', '^\s*$') . "k"<cr>

" Text objects to describe "falling" down, and then "falling" up. Equivalent to
" V<leader>kO<leader>j
onoremap <silent> ai <cmd>exec "normal! V" 
      \ . fall#fall('j', '^\s*$') 
      \ . "O"
      \ . fall#fall('k', '^\s*$') <cr>
onoremap <silent> ii <cmd>exec "normal! V" 
      \ . fall#fall('j', '^\s*$') 
      \ . "kO"
      \ . fall#fall('k', '^\s*$') . 'j' <cr>

vnoremap <silent> ai <cmd>exec "normal! " 
      \ . fall#fall('j', '^\s*$') 
      \ . "O"
      \ . fall#fall('k', '^\s*$') <cr>

vnoremap <silent> ii <cmd>exec "normal! " 
      \ . fall#fall('j', '^\s*$') 
      \ . "kO"
      \ . fall#fall('k', '^\s*$') . 'j' <cr>

vnoremap <expr> <silent> ic fall#visual_same_character("jk")
onoremap <silent> ic <cmd>exec "normal! V" . fall#visual_same_character("jk")<cr>

vnoremap <expr> <silent> ijc fall#visual_same_character("j")
onoremap <silent> ijc <cmd>exec "normal! V" . fall#visual_same_character("j")<cr>

vnoremap <expr> <silent> ikc fall#visual_same_character("k")
onoremap <silent> ikc <cmd>exec "normal! V" . fall#visual_same_character("k")<cr>
