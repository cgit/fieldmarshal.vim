" Remap 'Y' in visual mode to yank-and-join the lines. Useful when yanking
" something from the termial which is supposed to be a single line, but because
" of limitations in the termial, appear in serveral lines.

function! JoinYank()
  let reg = v:register
  exec printf('norm "%sy', reg)

  let new = substitute(getreg(reg), "\n", "", "g")
  call setreg(reg, new)
endfunction

vnoremap Y <cmd>call JoinYank()<cr>
