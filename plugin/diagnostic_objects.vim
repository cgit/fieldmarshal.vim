" Contains object mappings for vim diagnostics.

" indw: next diagnostic warning
" ildw: last diagnostic warning
" inde: next diagnostic error
" ilde: last diagnostic error
" indi: next diagnostic info
" ildi: last diagnostic info
" inda: next any diagnostic
" ilda: last any diagnostic

if ! has('nvim')
  " Must have neovim for this to work.
  finish
endif

onoremap indw <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 2)<cr>
onoremap ildw <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('last', 2)<cr>
onoremap inde <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 1)<cr>
onoremap ilde <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('last', 1)<cr>
onoremap indi <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 4)<cr>
onoremap ildi <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 4)<cr>
onoremap inda <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', -1)<cr>
onoremap ilda <cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', -1)<cr>

vnoremap indw <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 2)<cr>
vnoremap ildw <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('last', 2)<cr>
vnoremap inde <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 1)<cr>
vnoremap ilde <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('last', 1)<cr>
vnoremap indi <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 4)<cr>
vnoremap ildi <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', 4)<cr>
vnoremap inda <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', -1)<cr>
vnoremap ilda <esc><cmd>lua require('diagnostic_objects').highlight_matching_diagnostic('next', -1)<cr>
