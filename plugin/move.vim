onoremap <silent> a% V<cmd>call search('{') <bar> normal! %<cr>
onoremap <silent> i% <cmd>call search() <bar> normal! v%<bs>o<space><cr>

vnoremap <silent> a% <esc>V/{<cr>%
vnoremap <silent> i% <esc>/{<cr>v%<bs>o<space>o

"
" anb, anB, an<, an[
"
" "Next" objects. These objects are like ab, aB, a<, a[, etc. but will always go to the next body
" regardless of if it is nested or not.
"
onoremap <silent> anb <cmd>call <sid>inner(v:operator, 'a', '(', ')', v:false)<cr>
onoremap <silent> inb <cmd>call <sid>inner(v:operator, 'i', '(', ')', v:false)<cr>
vnoremap <silent> anb <cmd>call <sid>inner(v:operator, 'a', '(', ')', v:false)<cr>
vnoremap <silent> inb <cmd>call <sid>inner(v:operator, 'i', '(', ')', v:false)<cr>

onoremap <silent> anB <cmd>call <sid>inner(v:operator, 'a', '{', '}', v:false)<cr>
onoremap <silent> inB <cmd>call <sid>inner(v:operator, 'i', '{', '}', v:false)<cr>
vnoremap <silent> anB <cmd>call <sid>inner(v:operator, 'a', '{', '}', v:false)<cr>
vnoremap <silent> inB <cmd>call <sid>inner(v:operator, 'i', '{', '}', v:false)<cr>

onoremap <silent> an< <cmd>call <sid>inner(v:operator, 'a', '<', '>', v:false)<cr>
onoremap <silent> in< <cmd>call <sid>inner(v:operator, 'i', '<', '>', v:false)<cr>
vnoremap <silent> an< <cmd>call <sid>inner(v:operator, 'a', '<', '>', v:false)<cr>
vnoremap <silent> in< <cmd>call <sid>inner(v:operator, 'i', '<', '>', v:false)<cr>

onoremap <silent> an[ <cmd>call <sid>inner(v:operator, 'a', '\[', '\]', v:false)<cr>
onoremap <silent> in[ <cmd>call <sid>inner(v:operator, 'i', '\[', '\]', v:false)<cr>
vnoremap <silent> an[ <cmd>call <sid>inner(v:operator, 'a', '\[', '\]', v:false)<cr>
vnoremap <silent> in[ <cmd>call <sid>inner(v:operator, 'i', '\[', '\]', v:false)<cr>

" Change 'last' objects.
onoremap <silent> alb <cmd>call <sid>inner(v:operator, 'a', '(', ')', v:true)<cr>
onoremap <silent> ilb <cmd>call <sid>inner(v:operator, 'i', '(', ')', v:true)<cr>
vnoremap <silent> alb <cmd>call <sid>inner(v:operator, 'a', '(', ')', v:true)<cr>
vnoremap <silent> ilb <cmd>call <sid>inner(v:operator, 'i', '(', ')', v:true)<cr>

onoremap <silent> alB <cmd>call <sid>inner(v:operator, 'a', '{', '}', v:true)<cr>
onoremap <silent> ilB <cmd>call <sid>inner(v:operator, 'i', '{', '}', v:true)<cr>
vnoremap <silent> alB <cmd>call <sid>inner(v:operator, 'a', '{', '}', v:true)<cr>
vnoremap <silent> ilB <cmd>call <sid>inner(v:operator, 'i', '{', '}', v:true)<cr>

onoremap <silent> al< <cmd>call <sid>inner(v:operator, 'a', '<', '>', v:true)<cr>
onoremap <silent> il< <cmd>call <sid>inner(v:operator, 'i', '<', '>', v:true)<cr>
vnoremap <silent> al< <cmd>call <sid>inner(v:operator, 'a', '<', '>', v:true)<cr>
vnoremap <silent> il< <cmd>call <sid>inner(v:operator, 'i', '<', '>', v:true)<cr>

onoremap <silent> al[ <cmd>call <sid>inner(v:operator, 'a', '\[', '\]', v:true)<cr>
onoremap <silent> il[ <cmd>call <sid>inner(v:operator, 'i', '\[', '\]', v:true)<cr>
vnoremap <silent> al[ <cmd>call <sid>inner(v:operator, 'a', '\[', '\]', v:true)<cr>
vnoremap <silent> il[ <cmd>call <sid>inner(v:operator, 'i', '\[', '\]', v:true)<cr>

" ThisIsATest
function! s:inner(operator, ai, open, close, back) abort
  let opos = getpos('.')
  let i = 0

  if a:back
    while i < v:count + 1
      call search(a:close, 'b')
      let i += 1
    endwhile
  else
    while i < v:count + 1
      call search(a:open)
      let i += 1
    endwhile
  endif

  normal! %
  if (getline('.')[col('.')-1] =~ a:open)
    normal! %
  endif

  if col('.') > 1 && (getline('.')[col('.')-2] =~ a:open) && a:ai == 'i'
    if v:operator =~ "[cd]"
      " Cheese a 0-width by inserting a space to then immediately delete for d and c operators.
      exec "normal! i \<esc>v"
    elseif v:operator =~ "[y]"
      " Yank operation, don't do anything.
      call setpos('.', opos)
    else
      let [a, l, c, b] = getpos(".")
      call setpos("'<", [a, l, c, b])
      call setpos("'>", [a, l, c-1, b])
      normal! gv
    endif
    return
  endif

  if a:ai == 'i'
    exec "normal! \<esc>v%\<space>o\<bs>"
  else
    exec "normal! \<esc>v%"
  endif
endfunction
