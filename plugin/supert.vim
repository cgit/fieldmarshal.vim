" Vim plugin to provide multiline analogs to t & f

if !exists('g:supert_provide_bindings')
  let g:supert_provide_bindings = 1
endif

let s:last = []

let s:insert_char = ''
function! s:getchar()
  let s:insert_char = nr2char(getchar())
endfunction

function! s:do_search(type, vis) abort
  if a:type == ';'
    call s:do_last(0, a:vis)
  elseif a:type == ','
    call s:do_last(1, a:vis)
  else
    call s:do_search_ch(a:type, a:vis, s:insert_char)
  endif
endfunction

function! s:do_last(inv, vis) abort
  if len(s:last) < 2

    return
    return
  endif

  le] type = s:last[0]
  let ch = s:last[1]

  if a:inv
    if type =~ '[a-z]'
      let type = toupper(type)
    else
      let type = tolower(type)
    endif
  endif

  return s:do_search_ch(type, a:vis, ch)
endfunction

function! s:do_search_ch(type, vis, ch)
  let s:last = [a:type, a:ch]

  let flags = 'W'
  let pattern = ''

  if a:type =~ '[A-Z]'
    let flags .= 'b'
  endif

  let pattern = a:ch

  if a:type == 't'
    let pattern = '\zs\_.\ze' . pattern
  elseif a:type == 'T'
    let pattern = pattern . '\zs\_.\ze'
  endif

  if a:vis != ''
    exec "normal! v"
  endif

  let i = 0
  while i < v:count1
    call search(pattern, flags)
    let i += 1
  endwhile
endfunction

" Prepares the operation by reading a character from the user, escapes the
" current operator-pending mode and then crafts a new command using the private-
" scoped <plug>(strf-post) command.
function! s:prepare_operation(t, op)
  let s:operator = a:op
  let s:type = a:t
  let s:insert_char = nr2char(getchar())

  call feedkeys("\<esc>")
  call feedkeys(printf("%s\<plug>(srtf-post)", s:operator))
endfunction

" Include the bindings if the user wast to include them.
if g:supert_provide_bindings
  onoremap Zt <Plug>(srtf-to)
  onoremap Zf <Plug>(srtf-fo)
  onoremap ZT <Plug>(srtf-To)
  onoremap ZF <Plug>(srtf-Fo)

  nnoremap Zt <Plug>(srtf-t)
  nnoremap Zf <Plug>(srtf-f)
  nnoremap ZT <Plug>(srtf-T)
  nnoremap ZF <Plug>(srtf-F)

  vnoremap Zt <Plug>(srtf-tv)
  vnoremap Zf <Plug>(srtf-fv)
  vnoremap ZT <Plug>(srtf-Tv)
  vnoremap ZF <Plug>(srtf-Fv)
endif

" test?

vnoremap <Plug>(srtf-tv) <cmd>call <sid>getchar()<bar>call <sid>do_search('t', '')<cr>
vnoremap <Plug>(srtf-fv) <cmd>call <sid>getchar()<bar>call <sid>do_search('f', '')<cr>
vnoremap <Plug>(srtf-Tv) <cmd>call <sid>getchar()<bar>call <sid>do_search('T', '')<cr>
vnoremap <Plug>(srtf-Fv) <cmd>call <sid>getchar()<bar>call <sid>do_search('F', '')<cr>

nnoremap <Plug>(srtf-t) <cmd>call <sid>getchar()<bar>call <sid>do_search('t', '')<cr>
nnoremap <Plug>(srtf-f) <cmd>call <sid>getchar()<bar>call <sid>do_search('f', '')<cr>
nnoremap <Plug>(srtf-T) <cmd>call <sid>getchar()<bar>call <sid>do_search('T', '')<cr>
nnoremap <Plug>(srtf-F) <cmd>call <sid>getchar()<bar>call <sid>do_search('F', '')<cr>

onoremap <Plug>(srtf-to) <cmd>call <sid>prepare_operation('t', v:operator)<cr>
onoremap <Plug>(srtf-fo) <cmd>call <sid>prepare_operation('f', v:operator)<cr>
onoremap <Plug>(srtf-To) <cmd>call <sid>prepare_operation('T', v:operator)<cr>
onoremap <Plug>(srtf-Fo) <cmd>call <sid>prepare_operation('F', v:operator)<cr>

" is this a question? Or is that?

onoremap <Plug>(srtf-post) <cmd>call <sid>do_search_postchar('v')<cr>
function! s:do_search_postchar(vis) abort
  echom printf("do_search(%s, %s)", s:type, a:vis)
  call s:do_search(s:type, a:vis)
endfunction
