" Monocole.vim: creates folds around a text object, such that only that text
" object is visible.
"
" This is useful when working in a very big file and only want a portion of that
" file visible at a time.
if !exists('g:monocole_create_mappings')
  let g:monocole_create_mappings = 1
endif

" Create a monocole from normal mode. This is meant to work with operatorfunc.
function! s:create_monocole(text) abort
  set foldmethod=manual
  silent! norm zE

  0,'[-1fold
  silent! ']+1,$fold
endfunction

" Creates a monocole around a visual block.
function! s:v_create_monocole() abort
  set foldmethod=manual
  silent! norm zE

  0,'<-1fold
  silent! '>+1,$fold
endfunction

noremap <silent> <Plug>(monocole-create) :<C-u>silent!set operatorfunc=<SID>create_monocole<cr>g@
vnoremap <silent> <Plug>(v-monocole-create) :<C-u>call <SID>v_create_monocole()<cr>

if g:monocole_create_mappings
  noremap <silent> z<C-f> <Plug>(monocole-create)
  vnoremap <silent> z<C-f> <Plug>(v-monocole-create)
endif
