" subwords.vim. Motions and text objects for dealing with subwords.
"
" A subword is a word within an identifier, like a word within a camelCase
" identifer.
"

" Text object _. This references a "subword" 'a_' is around, which in the case
" of snake_case identifier will include the underscores (_). The 'i_' references
" the ci_

onoremap <silent> <Plug>(inner-sub-word) <cmd>exec "norm " . subwords#visual(v:true, v:false)<cr>
vnoremap <silent> <Plug>(inner-sub-word) <cmd>exec "norm " . subwords#visual(v:true, v:false)<cr>
onoremap <silent> <Plug>(around-sub-word) <cmd>exec "norm " . subwords#visual(v:true, v:true)<cr>
vnoremap <silent> <Plug>(around-sub-word) <cmd>exec "norm " . subwords#visual(v:true, v:true)<cr>

if ! exists('g:subwords_include_bindings')
  let g:subwords_include_bindings = 1
endif

" These mappings are the same as above, except prefer_camel is turned off, so
" snake case is used in the case of a conflict.
onoremap <silent> <Plug>(inner-sub-word-prefer-snake) <cmd>exec "norm " . subwords#visual(v:false, v:false)<cr>
vnoremap <silent> <Plug>(inner-sub-word-prefer-snake) <cmd>exec "norm " . subwords#visual(v:false, v:false)<cr>
onoremap <silent> <Plug>(around-sub-word-prefer-snake) <cmd>exec "norm " . subwords#visual(v:false, v:true)<cr>
vnoremap <silent> <Plug>(around-sub-word-prefer-snake) <cmd>exec "norm " . subwords#visual(v:false, v:true)<cr>

" Movement keys for subwords. These all have prefer_camel set to true, the idea
" being it's pretty easy to navigate underscores with f_ and t_, but more
" difficult to navigate upper case letters.
noremap   <silent> <Plug>(next-subword)   <cmd>silent! call subwords#next(v:false, v:true)<cr>
noremap   <silent> <Plug>(prev-subword)   <cmd>silent! call subwords#next(v:false, v:false)<cr>
vnoremap  <silent> <Plug>(next-subword)   <cmd>silent! call subwords#next(visualmode(), v:true)<cr>
vnoremap  <silent> <Plug>(prev-subword)   <cmd>silent! call subwords#next(visualmode(), v:false)<cr>

noremap  <expr> <silent> <Plug>(subwords-replace-;) subwords#repeat(';')
noremap  <expr> <silent> <Plug>(subwords-replace-,) subwords#repeat(',')
vnoremap <expr> <silent> <Plug>(subwords-replace-;) subwords#repeat(';')
vnoremap <expr> <silent> <Plug>(subwords-replace-,) subwords#repeat(',')

noremap <expr> <silent> <Plug>(subwords-replace-t) subwords#clear_mark() . "t" 
noremap <expr> <silent> <Plug>(subwords-replace-f) subwords#clear_mark() . "f" 
noremap <expr> <silent> <Plug>(subwords-replace-T) subwords#clear_mark() . "T"
noremap <expr> <silent> <Plug>(subwords-replace-F) subwords#clear_mark() . "F" 

if g:subwords_include_bindings
  onoremap <silent> i_ <Plug>(inner-sub-word-prefer-snake)
  vnoremap <silent> i_ <Plug>(inner-sub-word-prefer-snake)
  onoremap <silent> a_ <Plug>(around-sub-word-prefer-snaked)
  vnoremap <silent> a_ <Plug>(around-sub-word-prefer-snaked)

  onoremap <silent> i- <Plug>(inner-sub-word)
  vnoremap <silent> i- <Plug>(inner-sub-word)
  onoremap <silent> a- <Plug>(around-sub-word)
  vnoremap <silent> a- <Plug>(around-sub-word)

  noremap  + <Plug>(next-subword)
  vnoremap + <Plug>(next-subword)
  noremap  - <Plug>(prev-subword)
  vnoremap - <Plug>(prev-subword)

  noremap  <silent> ; <Plug>(subwords-replace-;)
  noremap  <silent> , <Plug>(subwords-replace-,)
  vnoremap <silent> ; <Plug>(subwords-replace-;)
  vnoremap <silent> , <Plug>(subwords-replace-,)

  noremap <silent> t <Plug>(subwords-replace-t)
  noremap <silent> T <Plug>(subwords-replace-T)
  noremap <silent> f <Plug>(subwords-replace-f)
  noremap <silent> F <Plug>(subwords-replace-F)
endif
