function! s:do_window_swap() abort
  let cur_buffer = bufnr('%')
  let cur_win = win_getid()

  let expr = ""
  let c = nr2char(getchar())
  while c =~ '[0-9]'
    let expr = expr .. c
    let c = nr2char(getchar())
  endwhile
  let expr = expr .. c

  let w = winnr(expr)
  if w > 0
    let wid = win_getid(w)
    let buf = nvim_win_get_buf(wid)

    call nvim_win_set_buf(wid, cur_buffer)
    call nvim_win_set_buf(cur_win, buf)

    exec w .. "wincmd w"
  endif

endfunction

noremap <Plug>(WindowSwap) <cmd>call <SID>do_window_swap()<cr>
noremap <C-w><C-S> <Plug>(WindowSwap)
noremap <C-S-w> <Plug>(WindowSwap)
