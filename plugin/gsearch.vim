" Search within a text object. Useful for finding variables scoped to a loop or
" a function or something.

function! s:search_within(t)
  if a:t == "char"
    let vtype = 'v'
  elseif a:t == "block"
    let vtype = ''
  else
    let vtype = 'V'
  endif


  call feedkeys('`[' .. vtype .. '`]/\%V', 'n')
endfunction

" g/ followed by a text object will start a search, but limit it to the lines
noremap <silent> g/ <cmd>set operatorfunc=<sid>search_within<cr>g@
vnoremap <silent> g/ <esc>/\%V
