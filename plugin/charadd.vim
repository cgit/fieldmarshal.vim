" Add/substracts a number from all the characters in a text object.
"
" [count]cx<obj>  - increment all the characters in the text object by count,
"                   except whitespace
" [count]cX<obj>  - decrement all characters in the text object by count, except
"                   whitespace
" [count]cgx<obj> - like cx, but include whitespace characters
" [count]cgX<obj> - like cX, but include whitespace characters
"
" The following convinence functions are added
"
" cxx, cXX, cgxx, cgXX to operate on lines.
"

if !exists('g:charadd_include_bindings')
  let g:charadd_include_bindings = 1
endif

noremap  <silent> <Plug>(add-char)      <cmd>call <sid>set_dir( v:count1,0)<bar>set operatorfunc=<sid>charadd<cr>g@
nnoremap <silent> <Plug>(add-char-line) <cmd>call <sid>set_dir( v:count1,0)<bar>set operatorfunc=<sid>charadd<cr>g@_
noremap  <silent> <Plug>(sub-char)      <cmd>call <sid>set_dir(-v:count1,0)<bar>set operatorfunc=<sid>charadd<cr>g@
nnoremap <silent> <Plug>(sub-char-line) <cmd>call <sid>set_dir(-v:count1,0)<bar>set operatorfunc=<sid>charadd<cr>g@_

noremap  <silent> <Plug>(add-char-all)      <cmd>call <sid>set_dir( v:count1,1)<bar>set operatorfunc=<sid>charadd<cr>g@
nnoremap <silent> <Plug>(add-char-all-line) <cmd>call <sid>set_dir( v:count1,1)<bar>set operatorfunc=<sid>charadd<cr>g@_
noremap  <silent> <Plug>(sub-char-all)      <cmd>call <sid>set_dir(-v:count1,1)<bar>set operatorfunc=<sid>charadd<cr>g@
nnoremap <silent> <Plug>(sub-char-all-line) <cmd>call <sid>set_dir(-v:count1,1)<bar>set operatorfunc=<sid>charadd<cr>g@_

if g:charadd_include_bindings
  noremap cx  <Plug>(add-char)
  noremap cxx <Plug>(add-char-line)
  noremap cX  <Plug>(sub-char)
  noremap cXX <Plug>(sub-char-line)

  noremap cgx  <Plug>(add-char-all)
  noremap cgxx <Plug>(add-char-all-line)
  noremap cgX  <Plug>(sub-char-all)
  noremap cgXX <Plug>(sub-char-all-line)
endif

let s:dir = 1
let s:incl = 0
function! s:set_dir(d, i) abort
  let s:dir = a:d
  let s:incl = a:i
endfunction

function! s:charadd(arg, ...) abort
  let cb = {'as_chars': 1}

  function cb.operate(l, ...)
    let nl = ""
    for c in a:l
      if s:incl || ! (c =~ '\_s')
        let n = char2nr(c)
        let nl .= nr2char(n + s:dir)
      else
        let nl .= c
      endif
    endfor
    return nl
  endfunction

  call fieldmarshal#modifytext(a:arg, cb)
endfunction
