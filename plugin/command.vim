" g: last command operator. 
"
" Runs the last command over the given text object.
"
" By "last command" I mean the last command run with a range. This avoids all those pesky :w and :e
" commands.
"
" so now to join all lines in a text object one could do something like:
"
" vap:join<cr>
"
" now this command can be repeated around a different text object using g:, for example g:ab
"
" what's more is after using g:, it is dot-repeatable.
"
" Note that while this is powerful, it wil only work on line-style selections as that it a
" limitation of ex commands.

noremap <silent> g: :set operatorfunc=<SID>do_command_around<cr>g@
noremap <silent> g:: :set operatorfunc=<SID>do_command_around<cr>g@_

function! s:find_cmd() abort
  let match_mark = "\\('.\\)\\?[0-9\\-+^$.%]*"

  let i = -1
  while 1 
    let ocmd = histget(':', i)

    " Strip the leading range
    let ncmd = substitute(ocmd, printf('\(%s\)\(,\(%s\)\)\?', match_mark, match_mark), "", "")

    if ncmd != ocmd
      " If this command had a range, use that one.
      let cmd = ncmd
      break
    endif

    let i -= 1
  endwhile

  return cmd
endfunction

function! s:do_command_around(str) abort
  call setpos("'<", getpos("'["))
  call setpos("'>", getpos("']"))

  let cmd = s:find_cmd()


  echo printf("'<,'>%s", cmd)
  exec printf("'<,'>%s", cmd)
endfunction
