" A small plugin to replace a text object with the contents of a register.
"
" This is invoked with the 'cp' command. For example
"
"   cpiw - replace the inner word with the contents of the default regist
" "ncpiw - replace the inner word with the contents of register 'n'
"
" The cp command also has some standard variants
"
"   cpp - replace the whole line with the contents of the default register
"   cP  - replace from the current cursor position to th end of the line with
"   the  given register
"
" Notably, this command does not alter the contents of the default register, so
" this command may be repeated without worry for changing the editor state.

if !exists('g:put_include_bindings')
  let g:put_include_bindings = 1
endif

noremap <silent> <Plug>(copy-put) <cmd>call <sid>setcpreg(v:register)<bar>set operatorfunc=<sid>put<cr>g@
noremap <silent> <Plug>(copy-put-line) <cmd>call <sid>setcpreg(v:register)<bar>set operatorfunc=<sid>put<cr>g@_
noremap <silent> <Plug>(copy-put-end) <cmd>call <sid>setcpreg(v:register)<bar>set operatorfunc=<sid>put<cr>g@$

if g:put_include_bindings
  noremap cp  <Plug>(copy-put)
  noremap cpp <Plug>(copy-put-line)
  noremap cP  <Plug>(copy-put-end)
endif

" Like 'p', but in visual mode doesn't clobber the "-register. If invoked with a
" register, the replaced text is put in the given register.
vnoremap <silent> P <cmd>call <sid>paste_into_register(v:register)<cr>

function! s:paste_into_register(r) abort
  let save = getreg('"')
  normal! p
  call setreg(a:r, getreg('"'))
  call setreg('"', save)
endfunction

let s:savereg = ''
let s:cpbuf = '"'
function! s:setcpreg(r) abort
  let s:cpbuf = a:r
endfunction

function! s:put(arg, ...) abort

  if a:0
    echo a:1
    let vis = '`<' . a:arg . '`>'
  elseif a:arg == 'line'
    let vis = "'[V']"
  elseif a:arg == 'block'
    let vis = "`[\<C-v>`]"
  else
    let vis = "`[v`]"
  endif

  let save = getreg('"')
  let savetyp = getregtype('"')
  silent! exec printf("norm %s\"%sp", vis, s:cpbuf)
  call setreg('"', save, savetyp)

endfunction
