" Using <C-w>+, <C-w>-, <C-w><, <C-w>> without a number will enter a "resize"
" mode where the pane can be resized by repeatedly pressing those keys.

if !exists("g:vim_fieldmarshal_resize_skip")
  let g:vim_fieldmarshal_resize_skip = 3
endif

function! s:enter_resize_mode(chr) abort
  let c = a:chr
  if v:count != 0
    exec "wincmd " . c
    return
  endif

  exec g:vim_fieldmarshal_resize_skip . "wincmd " . c
  redraw

  let c = nr2char(getchar())
  while c == '<' || c == '>' || c == '+' || c == '-'
    exec g:vim_fieldmarshal_resize_skip . "wincmd " . c
    redraw
    let c = nr2char(getchar())
  endwhile
endfunction

noremap <C-w>+ <cmd>call <sid>enter_resize_mode('+')<cr>
noremap <C-w>- <cmd>call <sid>enter_resize_mode('-')<cr>
noremap <C-w>> <cmd>call <sid>enter_resize_mode('>')<cr>
noremap <C-w>< <cmd>call <sid>enter_resize_mode('<')<cr>
