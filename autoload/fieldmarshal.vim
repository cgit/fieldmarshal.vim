
" Modify the text described by the '[ and '] marks.
"
" @param cb the callback used to modify the text should have the following
"        function:
"
"          {operate} - takes a list and returns the modified list.
"
"
" This function does make temporary use of the @a register, and thus the operate
" function should not expect changes to that register to persist.
function! fieldmarshal#modifytext(type, cb) abort
  let save_a = getreg('a')
  let save_a_type = getregtype('a')

  " Yank the contents of the described text object into the "a register.
  if a:type == 'line'
    let vis = "'[V']"
  elseif a:type == 'block'
    let vis = "`[\<C-v>`]"
  else
    let vis = "`[v`]"
  endif
  silent! exec printf("norm! %s\"ay", vis)

  " Get the a register, 
  let ls = getreg('a', 1, !get(a:cb, 'as_chars', 0))
  let regtype = getregtype('a')

  let ls = a:cb.operate(ls, regtype)

  " Set the register list to whatever was returned from operate(). Then
  " re-VIsualize the text object and paste.
  call setreg('a', ls, regtype)
  silent! exec "norm! gv\"ap"

  " Restore the register "a to what it was before.
  call setreg('a', save_a, save_a_type)
endfunction
