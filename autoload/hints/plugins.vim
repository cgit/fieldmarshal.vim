let s:ftplugins = {}

let s:vim_plugin = {}
function! s:vim_plugin.Before(file)
endfunction
function! s:vim_plugin.TagLine(linenr, line)
  return a:line =~ '\<\(if\|function\|return\|end\w*\|while\|for\|let\|else\|try\)\>'
endfunction

let s:WHITESPACE_OR_COMMENT='\(^\s*$\)\|\(^\s*//\)\|\(^\s*\*/\)'
let s:java_plugin = {}
function! s:java_plugin.new() dict
  let ret = {}

  function! ret.Before(file) dict
    let self.last_line = ''
    let self.last_indent = ''
  endfunction

  function! ret.TagLine(linenr, line) dict
    if self.last_line =~ s:WHITESPACE_OR_COMMENT
          \ && !(a:line =~ s:WHITESPACE_OR_COMMENT)
      let self.last_line = a:line
      return v:true
    endif

    let self.last_line = a:line
    let indent = matchlist(a:line, '^\s*')
    let indent = len(indent) > 0 ? indent[0] : ""
    if self.last_indent != indent
      let self.last_indent = indent
      return v:true
    endif

    return 
          \ a:line =~ '^\s*}$' ||
          \ a:line =~ '\<\(public\|private\|protected\|class\|static\|try\|while\|for\|if\|else\|catch\)\>'
  endfunction

  return ret
endfunction

function! hints#plugins#registerFt(filetype, plugin) abort
  let s:ftplugins[a:filetype] = a:plugin
endfunction

function! hints#plugins#getPluginForFiletype(filetype) abort
  let plug = get(s:ftplugins, a:filetype, s:default_plugin)
  if has_key(plug, "new")
    let plug = plug.new()
  endif
  return plug
endfunction

let s:default_plugin = {}
function! hints#plugins#getDefaultPlugin() abort
  return s:default_plugin
endfunction


let s:ISSPACE = '^\s*$'
let s:ISCOMMENT = '^\s*[-/#;"(]'
function! s:default_plugin.new()
  let ret = {}

  function! ret.Before(file)
    let self.last_kind = 1
  endfunction

  function! ret.TagLine(linenr, line)
    if a:line =~ s:ISSPACE
      let kind = 1
    elseif a:line =~ s:ISCOMMENT
      let kind = 2
    else
      let kind = 3
    endif

    if self.last_kind != kind && kind != 1
      let self.last_kind = kind
      return v:true
    endif

    let self.last_kind = kind
    return v:false
  endfunction

  return ret
endfunction

call hints#plugins#registerFt("vim", s:vim_plugin)
call hints#plugins#registerFt("java", s:java_plugin)
