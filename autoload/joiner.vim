
" Function for the operatorfunc with joiner.
function! joiner#do(type, ...) abort
  if a:0
    silent exe "norm! gvy"
  elseif a:type == 'line'
    " yank the text described by the motion
    silent exe "norm! '[V']y"
  else
    silent exe "norm! `[v`]y"
  endif

  let yanked = getreg('"', 1, v:true)
  let yankedtype = getregtype('"')

  let changed = [g:JoinerJoinText(yanked, yankedtype)]

  call setreg('"', changed, yankedtype)

  norm gvp

  " Reset the yanked text to what it was originally.
  call setreg('"', yanked, yankedtype)
endfunction

function! joiner#join_with_string(yanked, yanktype) abort
  let join_string = input('> ')
  return join(map(a:yanked, "substitute(v:val, '^\\s\\+', '', '')"), join_string)
endfunction

function! joiner#join_with_string_literal(yanked, yanktype) abort
  let join_string = input('> ')
  return join(a:yanked, join_string)
endfunction

function! joiner#default_join(yanked, yanktype) abort
  return join(map(a:yanked, "substitute(v:val, '^\\s\\+', '', '')"), ' ')
endfunction
